import React from 'react';
import { View, Text } from 'react-native';
import { createStackNavigator } from 'react-navigation';

import HomeScreen from './src/views/Home';
import ProfileScreen from './src/views/Profile';
import ScanScreen from './src/views/Scanner';

export default createStackNavigator(
  {
    Home: HomeScreen,
    Profile: ProfileScreen,
    Scan: ScanScreen
  },
  {
    initialRouteName: 'Home',
    /* The header config from HomeScreen is now here */
    navigationOptions: {
      headerTitleStyle:{ textAlign:'center', alignSelf:'center',flex:1 }
    },
  }
);