import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity
} from 'react-native';
import QRCode from 'react-native-qrcode-svg';

const ListItem = (props) => {
    return (
        props.items.map((item, i) => (
            <TouchableOpacity 
                onPress={ () => props.show(i)}
                key={i}
                style={ styles.content }>
                <View>
                    <QRCode
                        value = { item }
                    />
                </View>
                <Text>{ item }</Text>
            </TouchableOpacity> 
        ))
    )
}

const styles =  StyleSheet.create({
    content: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'lightblue',
        borderWidth: 1,
        borderColor: 'black',
        width: '100%',
        padding: 3,
    }
});

export default ListItem;