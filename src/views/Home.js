import React, { Component } from 'react';
import {
    View,
    Text,
    Button,
    StyleSheet
} from 'react-native';
import ListItem from './partials/ListItem';
import Tickets from '../models/tickets';

import { connect } from 'react-redux';
import { addTicket, getTickets } from '../store/actions';
import { bindActionCreators } from 'redux';

import DeviceInfo from 'react-native-device-info';
import firebase from '../store/firebase';


const DEVICE_ID =  DeviceInfo.getUniqueID();

class HomeScreen extends Component {
    
    static navigationOptions = ({ navigation }) => {
        return {
            headerTitle: 'My Tickets',
            headerLeft: (
                <Button
                    onPress={ navigation.getParam('navToScan') }
                    title="Scan"
                />
            ),
            headerRight: (
                <Button
                    onPress={ navigation.getParam('navToGen') }
                    title="Gen"
                />
            ),
        };
    };

    navToScan = () => {
        this.props.navigation.navigate('Scan', { callback: this.onReceiveScan });
    }

    navToGen = () => {
        this.props.navigation.navigate('Scan', { callback: this.onReceiveGen });
    }

    navToProfile = (key) => {
        this.props.navigation.navigate('Profile', { qrCodeId: key, callback: this.onTransfer });
    }

    onTransfer = (e) => {
        this.props.getTickets();
    }

    onReceiveScan = (e) => {
        const MY_DEVICE_ID =  DeviceInfo.getUniqueID();

        let data = e.data.split('|');
        var ticketRef = firebase.database().ref(`${data[0]}/${data[1]}`);
        ticketRef.transaction(function(ticket) {
            if (ticket) {
              ticket['transfer_to'] = MY_DEVICE_ID;
            }
            return ticket;
        });
    }

    onReceiveGen = (e) => {
        this.props.addTicket({ ticket: e.data, create_at: Date(), transfer_to: "" });
    }

    render() {
        return (
            <View style={ styles.ListView }>
                {/* <ListItem items={ this.state.qrcode } show={ this.navToProfile }/> */}
                <Tickets show={ this.navToProfile } />
            </View>
        );
    }

    //
    componentDidMount() {
        this.props.navigation.setParams({ navToScan: this.navToScan });
        this.props.navigation.setParams({ navToGen: this.navToGen });
        var ticketRef = firebase.database().ref(`${DEVICE_ID}`);
        ticketRef.on('child_added', (data)=> {
            this.props.getTickets();
        });
    }
    
}

const styles = StyleSheet.create({
    ListView: { 
        flex: 1, 
        alignItems: 'flex-start', 
        justifyContent: 'flex-start', 
        backgroundColor: '#ccc' 
    }
});


function mapStateToProps(state) {
    return {
        tickets: state.tickets
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({addTicket, getTickets}, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);