import React, { Component } from 'react';
import {
    View,
    Text,
    Button,
    Modal
} from 'react-native';
import QRCode from 'react-native-qrcode-svg';

import { connect } from 'react-redux';
import { getTicket } from '../store/actions';
import { bindActionCreators } from 'redux';

import { ConfirmDialog } from 'react-native-simple-dialogs';

import DeviceInfo from 'react-native-device-info';
import firebase from '../store/firebase';

const DEVICE_ID =  DeviceInfo.getUniqueID();

class ProfileScreen extends Component {

    state = { showConfirm: false };

    openConfirm(show) {
        this.setState({ showConfirm: show });
    }

    optionYes = () => {
        this.openConfirm(false);
        // Get a key for a new Post.
        var newTicketKey = firebase.database().ref().child(`${this.state.transfer_to}`).push().key;
        // Write the new post's data simultaneously in the posts list and the user's post list.
        let newTicket = {
            ticket: this.props.ticket.ticket,
            create_at: Date(),
            transfer_to: ""
        }
        // Write the new post's data simultaneously in the posts list and the user's post list.
        var updates = {};
        updates[`/${this.state.transfer_to}/` + newTicketKey] = newTicket;
        
        firebase.database().ref().update(updates, (error) => {
            if (error) {
                // The write failed...
              } else {
                // Data saved successfully!
                firebase.database().ref(`${DEVICE_ID}/${this.props.navigation.getParam('qrCodeId')}`).remove();
                this.props.navigation.goBack();
                const callback = this.props.navigation.getParam('callback');
                callback();
              }
        });
    }

    optionNo = () => {
        this.openConfirm(false);
        
        var ticketRef = firebase.database().ref(`${DEVICE_ID}/${this.props.navigation.getParam('qrCodeId')}`);
        ticketRef.transaction(function(ticket) {
            if (ticket) {
                ticket['transfer_to'] = "";
            }
            return ticket;
        });
    }

    componentDidMount() {
        this.props.getTicket(this.props.navigation.getParam('qrCodeId'));
        //
        var ticketRef = firebase.database().ref(`${DEVICE_ID}/${this.props.navigation.getParam('qrCodeId')}`);
        ticketRef.on('child_changed', (data)=> {
            if (data.val()) {
                this.setState({showConfirm: true});
                this.setState({transfer_to: data.val()});
            }
        });
    } 

    render() {
        const qrcode = this.props.ticket != null ? `${DEVICE_ID}|${this.props.ticket.id}` : 'empty';
        return (
            <View style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'center', paddingTop: 20 }}>
                <ConfirmDialog
                    title="Confirm Dialog"
                    message="Are you sure about that?"
                    visible={this.state.showConfirm}
                    onTouchOutside={() => this.openConfirm(false)}
                    positiveButton={{
                        title: "YES",
                        onPress: () => this.optionYes()
                    }}
                    negativeButton={{
                        title: "NO",
                        onPress: () => this.optionNo()
                    }}
                />
                <QRCode
                    value = { qrcode }
                    size = { 300 }
                />
            </View>
        );
    }
}


function mapStateToProps(state) {
    return {
        ticket: state.ticketsReducer.ticket
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({getTicket}, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen)