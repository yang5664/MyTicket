import axios from 'axios';
import DeviceInfo from 'react-native-device-info';

const DEVICE_ID =  DeviceInfo.getUniqueID();
const URL = `https://myticket-1dfbe.firebaseio.com/`;


export function getTickets() {
    const request = axios.get(`${URL}/${DEVICE_ID}.json`).then( response => {
        let tickets = [];

        for (let key in response.data) {
            tickets.push({
                ...response.data[key],
                id: key
            })
        }

        return tickets;
    });

    return {
        type: 'GET_TICKETS',
        payload: request
    }
}

export function getTicket(ticket_id) {
    const request = axios.get(`${URL}/${DEVICE_ID}/${ticket_id}.json`).then( response => {
        const ticket = {
            ...response.data, id: ticket_id
        }

        return ticket;
    });

    return {
        type: 'GET_TICKET',
        payload: request
    }
}

export function addTicket(data) {
    const request = axios.post(`${URL}/${DEVICE_ID}.json`, data).then( response => {
        const ticket = {
            ...data, id: response.data.name
        }

        return ticket;
    });

    return {
        type: 'ADD_TICKET',
        payload: request
    }

}