export default function(state={}, action) {
    switch (action.type) {
        case 'GET_TICKETS':
            return {...state, tickets: action.payload}
        case 'GET_TICKET':
            return {...state, ticket: action.payload}
        case 'ADD_TICKET':
            return {...state, addTicket: action.payload}
        default:
            return state;
    }
}