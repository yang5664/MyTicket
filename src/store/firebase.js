import * as firebase from "firebase";

const firebaseConfig = {
    apiKey: "AIzaSyCKKLbD8tCNkdPRDeqetnV_Sa_8q9fnBDU",
    authDomain: "myticket-1dfbe.firebaseapp.com",
    databaseURL: "https://myticket-1dfbe.firebaseio.com",
    storageBucket: "myticket-1dfbe.appspot.com",
};

export default !firebase.apps.length ? firebase.initializeApp(firebaseConfig) : firebase.app();