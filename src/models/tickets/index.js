import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity
} from 'react-native';
import QRCode from 'react-native-qrcode-svg';

import { connect } from 'react-redux';
import { getTickets } from '../../store/actions';
import { bindActionCreators } from 'redux';

class Tickets extends Component {

    componentDidMount() {
        this.props.getTickets();
    }

    render () {
        if (this.props.tickets != null && typeof(this.props.tickets) == 'object' ) {
            return (
                this.props.tickets.map((item, i) => (
                    <TouchableOpacity 
                        onPress={ () => this.props.show(item.id)}
                        key={item.id}
                        style={ styles.content }>
                        <View>
                            <QRCode
                                value = { item.ticket }
                            />
                        </View>
                        <Text>{ item.ticket }</Text>
                    </TouchableOpacity> 
                ))
            )
        } else {
            return (
                <View style={{  flex: 1, width: '100%', justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ fontSize: 30 }}>Refresh Ticket List</Text>
                </View>
            )
        }

    }
}

const styles =  StyleSheet.create({
    content: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'lightblue',
        borderWidth: 1,
        borderColor: 'black',
        width: '100%',
        padding: 3,
    }
});

function mapStateToProps(state) {
    return {
        tickets: state.ticketsReducer.tickets
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({getTickets}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Tickets);